from django.db import models
from djangodemo.models.CompanyType import CompanyType

#======================================================================
# 
# Encapsulates data for model Company
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class Company Declaration
#======================================================================
class Company (models.Model):

#======================================================================
# attribute declarations
#======================================================================
	name = models.CharField(max_length=200, null=True)
	employees = models.ManyToManyField('Employee',  blank=True, related_name='+')
	address = models.OneToOneField('Address', on_delete=models.CASCADE, null=True, blank=True, related_name='+')
	type = models.CharField(max_length=64, null=True, choices=[(tag.name, tag.value) for tag in CompanyType])

#======================================================================
# function declarations
#======================================================================
	def toString(self):
		str = ""
		str = str + self.name
		str = str + self.type
		return str;
    
	def __str__(self):
		return self.toString();

	def identity(self):
		return "Company";
    
	def objectType(self):
		return "Company";
