import json

from django.core import serializers
from django.shortcuts import render
from django.http import HttpResponse

from djangodemo.delegates.CompanyDelegate import CompanyDelegate

 #======================================================================
# 
# Encapsulates data for View Company
#
# @author Dev Team
#
#======================================================================

#======================================================================
# Class CompanyView function declarations
#======================================================================
def index(request):
	return HttpResponse("Hello, world. You're at the Company index.")

def get(request, companyId ):
	delegate = CompanyDelegate()
	responseData = delegate.get( companyId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def create(request):
	company = json.loads(request.body)
	delegate = CompanyDelegate()
	responseData = delegate.createFromJson( company )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def save(request):
	company = json.loads(request.body)
	delegate = CompanyDelegate()
	responseData = delegate.save( company )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def delete(request, companyId ):
	delegate = CompanyDelegate()
	responseData = delegate.delete( companyId )
	return HttpResponse(responseData, content_type="application/json");

def getAll(request):
	delegate = CompanyDelegate()
	responseData = delegate.getAll()
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def assignAddress( request, companyId, AddressId ):
	delegate = CompanyDelegate()
	responseData = delegate.saveAddress( companyId, AddressId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");
	
def unassignAddress( request, companyId ):
	delegate = CompanyDelegate()
	responseData = delegate.deleteAddress( companyId )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def addEmployees( request, companyId, EmployeesIds ):
	delegate = CompanyDelegate()
	responseData = delegate.addEmployees( companyId, EmployeesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

def removeEmployees( request, companyId, EmployeesIds ):
	delegate = CompanyDelegate()
	responseData = delegate.removeEmployees( companyId, EmployeesIds )
	asJson = serializers.serialize("json", responseData)
	return HttpResponse(asJson, content_type="application/json");

